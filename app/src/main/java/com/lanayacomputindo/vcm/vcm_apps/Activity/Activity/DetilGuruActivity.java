package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DetilGuruActivity extends AppCompatActivity {

    private TextView txtnama, txtnip, txtjabatan, txtpendidikan, txtalamat;

    private ImageView imgphoto;

    private String bundleid,bundlenama,bundlejabatan, bundletelp, bundleemail, bundlependidikan, bundlenip, bundletipe, bundlealamat,bundlephoto,bundletgl_create, bundlemerchant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_guru);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("");

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("id_pegawai");
        bundlenama = b.getString("nama_pegawai");
        bundlejabatan = b.getString("jabatan");
        bundletelp = b.getString("telp");
        bundleemail = b.getString("email");
        bundlependidikan = b.getString("pendidikan");
        bundlenip = b.getString("nip");
        bundletipe = b.getString("tipe");
        bundlealamat = b.getString("alamat");
        bundlephoto = b.getString("photo");
        bundletgl_create = b.getString("tgl_create");

        txtnama = (TextView) findViewById(R.id.nama);
        txtnip = (TextView) findViewById(R.id.nip);
        txtjabatan = (TextView) findViewById(R.id.jabatan);
        txtalamat = (TextView) findViewById(R.id.alamat);
        txtpendidikan = (TextView) findViewById(R.id.pendidikan);
        imgphoto = (ImageView) findViewById(R.id.photo);

        txtnama.setText(bundlenama);
        txtnip.setText(bundlenip);
        txtjabatan.setText(bundlejabatan);
        txtpendidikan.setText(bundlependidikan);
        txtalamat.setText(Html.fromHtml(bundlealamat));

        Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto.replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(imgphoto);

        imgphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DetilGambarActivity.class);

                Bundle b = new Bundle();

                b.putString("photo", bundlephoto);
                b.putString("namaproduk", bundlenama);
                b.putString("param", "pegawai");

                i.putExtras(b);

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(DetilGuruActivity.this, imgphoto, "detil");
                //Start the Intent
                ActivityCompat.startActivity(DetilGuruActivity.this, i, options.toBundle());

            }
        });

        FloatingActionButton emaill = (FloatingActionButton) findViewById(R.id.emaill);

        emaill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"+bundleemail));
                startActivity(Intent.createChooser(intent, "Pilih email anda"));
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                DetilGuruActivity.this.supportFinishAfterTransition();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}
