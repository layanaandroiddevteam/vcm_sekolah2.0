package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListJadwalAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListUndanganAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Invitation;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.SubjectTeacher;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentJadwalHarian extends Fragment {

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout ;

    ArrayList<SubjectTeacher> listUndangan = new ArrayList<>();

    Call<Results<Invitation>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    View v;

    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        sharedPreferences = getActivity().getSharedPreferences("shared",
                Activity.MODE_PRIVATE);

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSubjectToday();
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        getSubjectToday();

    }

    private void getSubjectToday() {
        service = RestClient.getClient(getActivity());

        String with = "subject;person;classRoom;classRoom.personClass";
        String search = "day:"+getArguments().getString("hari")+";classRoom.personClass.person_id:"+sharedPreferences.getInt("anak_id", 0);
        String orderBy = "start";
        String sortedBy = "asc";

        Call<Results<SubjectTeacher>> call = service.getSubjectTeacherToday(with, search, orderBy, sortedBy);
        call.enqueue(new Callback<Results<SubjectTeacher>>() {
            @Override
            public void onResponse(Call<Results<SubjectTeacher>> call, Response<Results<SubjectTeacher>> response) {
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Log.d("SubjectTeacher", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<SubjectTeacher>result = response.body();
                    Log.d("SubjectTeacher", "response = " + new Gson().toJson(result));
                    listUndangan = result.getData();
                    mAdapter = new ListJadwalAdapter(v.getContext(), listUndangan, getActivity());
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<Results<SubjectTeacher>> call,Throwable t) {
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Log.e("on Failure", t.toString());
                Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }
}