package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentAbsen;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentDashboard;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentJadwal;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentJadwalHarian;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentKalender;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentNews;
import com.lanayacomputindo.vcm.vcm_apps.R;

public class FragmentActivity extends AppCompatActivity {

	Fragment fragment = null;
	Class fragmentClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_survey);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
		getSupportActionBar().setHomeAsUpIndicator(upArrow);

		if(getIntent().getStringExtra("type").equalsIgnoreCase("nilai")){
			getSupportActionBar().setTitle("Nilai");

			fragmentClass = FragmentDashboard.class;
			try {
				fragment = (Fragment) fragmentClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
		}
		else if(getIntent().getStringExtra("type").equalsIgnoreCase("absen")){
			getSupportActionBar().setTitle("Absen");

			fragmentClass = FragmentAbsen.class;
			try {
				fragment = (Fragment) fragmentClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
		}
		else if(getIntent().getStringExtra("type").equalsIgnoreCase("kabar")){
			getSupportActionBar().setTitle(getString(R.string.kabar));

			fragmentClass = FragmentNews.class;
			try {
				fragment = (Fragment) fragmentClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
		}
		else if(getIntent().getStringExtra("type").equalsIgnoreCase("jadwal")){
			getSupportActionBar().setTitle("Jadwal");

			fragmentClass = FragmentJadwal.class;
			try {
				fragment = (Fragment) fragmentClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {

			case android.R.id.home:

				FragmentActivity.this.finish();

				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
