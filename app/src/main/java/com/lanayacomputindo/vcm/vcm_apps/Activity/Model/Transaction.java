package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Transaction {
    private Integer id;
    private Integer client_id;
    private String packet;
    private String expired;
    private String status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Client client;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The client_id
     */
    public Integer getClient_id() {
        return client_id;
    }

    /**
     *
     * @param client_id
     * The client_id
     */
    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    /**
     *
     * @return
     * The packet
     */
    public String getPacket() {
        return packet;
    }

    /**
     *
     * @param packet
     * The packet
     */
    public void setPacket(String packet) {
        this.packet = packet;
    }

    /**
     *
     * @return
     * The expired
     */
    public String getExpired() {
        return expired;
    }

    /**
     *
     * @param expired
     * The expired
     */
    public void setExpired(String expired) {
        this.expired = expired;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The client
     */
    public Client getClient() {
        return client;
    }

    /**
     *
     * @param client
     * The client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
