package com.lanayacomputindo.vcm.vcm_apps.Activity.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

public class PopupDialogGreetings extends Activity {

    private String bundleid, bundlejudul, bundleisi, bundlestatus, bundlephoto, bundlecreate;

    private AlertDialog notifdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_notification_dialog);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("id_greeting");
        bundlejudul = b.getString("judul");
        bundleisi = b.getString("isi");
        bundlestatus = b.getString("status");
        bundlephoto = b.getString("photo");
        bundlecreate = b.getString("create");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);


//        new MaterialStyledDialog(this)
//                .withIconAnimation(false)
//                .withDialogAnimation(true)
//                .setCancelable(false)
//                .setTitle(bundlenama)
//                .setDescription(tempdesc)
//                .setIcon(R.drawable.logo_merchant)
//                .setHeaderDrawable(R.color.warna_utama)
//                .setPositive("Lihat", new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                        Intent intent = new Intent(getApplicationContext(), DetilDiskonActivity.class);
//
//                        Bundle b = new Bundle();
//                        b.putString("idproduk", bundleid);
//                        b.putString("namaproduk", bundlenama);
//                        b.putString("status", bundlestatus);
//                        b.putString("deskripsi", bundledeskripsi);
//                        b.putString("tglmulai", bundletglmulai);
//                        b.putString("tglselesai", bundletglselesai);
//                        b.putString("harga", bundleharga);
//                        b.putString("diskon", bundlediskon);
//                        b.putString("photo", bundlephoto);
//                        b.putString("tglcreate", bundletglcreate);
//                        b.putString("merchant", bundlemerchant);
//
//                        intent.putExtras(b);
//
//                        startActivity(intent);
//
//                        finish();
//                    }
//                })
//                .setNegative("Tutup", new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                        finish();
//                    }
//                })
//                .show();

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialognotifgreeting, null);
        dialogBuilder.setView(dialogView);

        ImageView img = (ImageView) dialogView.findViewById(R.id.image);
        TextView txtdeskripsi = (TextView) dialogView.findViewById(R.id.deskripsi);
        TextView txtparam = (TextView) dialogView.findViewById(R.id.param);
        Button btnTutup = (Button) dialogView.findViewById(R.id.btntutup);

        Log.d("photo",bundlephoto);
        Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto.replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(img);

        txtdeskripsi.setText(bundleisi);
        txtparam.setText(bundlejudul);



        btnTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        notifdialog = dialogBuilder.create();
        notifdialog.setCancelable(false);
        notifdialog.show();

    }


}
