package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.Date;

public class DetilSurveyActivity extends AppCompatActivity {

	private String bundleid_survey, bundlejudul, bundlelink_url, bundletgl_create,	bundlestatus, bundlemsg;
	private WebView view;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detil_survey);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
		getSupportActionBar().setHomeAsUpIndicator(upArrow);

		Bundle b = getIntent().getExtras();

		bundleid_survey = b.getString("id_survey");
		bundlejudul = b.getString("judul");
		bundlelink_url = b.getString("link_url");
		bundletgl_create = b.getString("tgl_create");
		bundlestatus = b.getString("status");
		bundlemsg = b.getString("msg");

		Log.e("link", bundlelink_url);

		getSupportActionBar().setTitle(bundlejudul);

		view = (WebView) this.findViewById(R.id.webview);

		getWindow().setFlags(
			WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
			WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

		view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

		view.getSettings().setJavaScriptEnabled(true);
		view.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
		view.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		view.setWebViewClient(new MyBrowser());
		view.loadUrl(bundlelink_url);

	}


	private class MyBrowser extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//ketika disentuh tombol back
		if ((keyCode == KeyEvent.KEYCODE_BACK) && view.canGoBack()) {
			view.goBack(); //method goback() dieksekusi untuk kembali pada halaman sebelumnya
			return true;
		}
		// Jika tidak ada history (Halaman yang sebelumnya dibuka)
		// maka akan keluar dari activity
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {

			case android.R.id.home:

				DetilSurveyActivity.this.finish();

				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
