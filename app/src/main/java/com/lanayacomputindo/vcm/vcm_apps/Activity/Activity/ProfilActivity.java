package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfilActivity extends AppCompatActivity {

    private TextView txtnama, txtemail, txtalamat, txtdeskripsi;
    Call<Results<Branch>> call;
    RestClient.GitApiInterface service;

    String telp;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Profil Sekolah");

        txtnama = (TextView) findViewById(R.id.nama);
        txtemail = (TextView) findViewById(R.id.email);
        txtalamat = (TextView) findViewById(R.id.alamat);
        txtdeskripsi = (TextView) findViewById(R.id.deskripsi);

        getProfil();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ( ContextCompat.checkSelfPermission(ProfilActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ) {

                    ActivityCompat.requestPermissions(ProfilActivity.this, new String[]{android.Manifest.permission.CALL_PHONE},
                            1);
                }
                else
                {
                    String uri = "tel:" + telp.trim() ;
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                }

            }
        });

        fab.setVisibility(View.GONE);

    }

    public void getProfil()
    {
        service = RestClient.getClient(this);
        call = service.getClientBranch();

        call.enqueue(new Callback<Results<Branch>>() {
            @Override
            public void onResponse(Call<Results<Branch>> call,  Response<Results<Branch>> response) {
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Branch> result = response.body();
                    Log.d("MainActivity", "response = " + new Gson().toJson(result));
                    txtnama.setText(result.getData().get(0).getName());
                    txtemail.setText(result.getData().get(0).getEmail());
                    txtalamat.setText(result.getData().get(0).getAddress());
                    txtdeskripsi.setText(Html.fromHtml(result.getData().get(0).getDescription()));
                    telp = result.getData().get(0).getPhone();
                    fab.setVisibility(View.VISIBLE);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.e("on Failure", "asas");

                }
            }

            @Override
            public void onFailure(Call<Results<Branch>> call,Throwable t) {
                Log.d("MainActivity", "gagalall");
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                ProfilActivity.this.finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
