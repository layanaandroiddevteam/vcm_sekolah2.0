package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pradana on 01/09/2016.
 */
public class Update {
    private Integer id;
    private Integer client_id;
    private String version_code;
    private String version_name;
    private String url;
    private String gcm_server;
    private Boolean required;
    private String description;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The client_id
     */
    public Integer getClient_id() {
        return client_id;
    }

    /**
     *
     * @param client_id
     * The client_id
     */
    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    /**
     *
     * @return
     * The version_code
     */
    public String getVersion_code() {
        return version_code;
    }

    /**
     *
     * @param version_code
     * The version_code
     */
    public void setVersion_code(String version_code) {
        this.version_code = version_code;
    }

    /**
     *
     * @return
     * The version_name
     */
    public String getVersion_name() {
        return version_name;
    }

    /**
     *
     * @param version_name
     * The version_name
     */
    public void setVersion_name(String version_name) {
        this.version_name = version_name;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The required
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     *
     * @param required
     * The required
     */
    public void setRequired(Boolean required) {
        this.required = required;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getGcm_server() {
        return gcm_server;
    }

    public void setGcm_server(String gcm_server) {
        this.gcm_server = gcm_server;
    }
}
