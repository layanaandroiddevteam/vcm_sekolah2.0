package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.lanayacomputindo.vcm.vcm_apps.R;

/**
 * Created by Yoshua on 11/14/2015.
 */
public class FragmentPreferences extends PreferenceFragment {

    @Override
    public void onCreate(final Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_setting);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean checkBoxDiskon = prefs.getBoolean("set_notif_undangan", true);
        boolean checkBoxKabar = prefs.getBoolean("set_notif_kabar", true);

        if(checkBoxDiskon)
        {
            Preference myPref = findPreference("set_notif_undangan");
            CheckBoxPreference check = (CheckBoxPreference)myPref;
            check.setChecked(true);
        }
        else
        {
            Preference myPref = findPreference("set_notif_undangan");
            CheckBoxPreference check = (CheckBoxPreference)myPref;
            check.setChecked(false);
        }

        if(checkBoxKabar)
        {
            Preference myPref = findPreference("set_notif_kabar");
            CheckBoxPreference check = (CheckBoxPreference)myPref;
            check.setChecked(true);
        }
        else
        {
            Preference myPref = findPreference("set_notif_kabar");
            CheckBoxPreference check = (CheckBoxPreference)myPref;
            check.setChecked(false);
        }

//        Preference myPref = findPreference("is_login");
//        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            @Override
//            public boolean onPreferenceClick(Preference preference) {
//
//                getActivity().finish();
//                return false;
//            }
//        });
//
//        Preference about = findPreference("about");
//        about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            @Override
//            public boolean onPreferenceClick(Preference preference) {
//
//                return false;
//            }
//        });
    }
}