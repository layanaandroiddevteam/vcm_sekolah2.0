package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pradana on 13/09/2016.
 */
public class Invitation {
    private Integer id;
    private Integer user_id;
    private Integer post_id;
    private Boolean value;
    private String detail;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Post post;
    private User user;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     *
     * @param user_id
     * The user_id
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     *
     * @return
     * The post_id
     */
    public Integer getPost_id() {
        return post_id;
    }

    /**
     *
     * @param post_id
     * The post_id
     */
    public void setPost_id(Integer post_id) {
        this.post_id = post_id;
    }

    /**
     *
     * @return
     * The value
     */
    public Boolean getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(Boolean value) {
        this.value = value;
    }

    /**
     *
     * @return
     * The detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     *
     * @param detail
     * The detail
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The post
     */
    public Post getPost() {
        return post;
    }

    /**
     *
     * @param post
     * The post
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
