package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentPreferences;
import com.lanayacomputindo.vcm.vcm_apps.R;

public class PengaturanActivity extends ActionBarActivity {



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Pengaturan");

        getFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentPreferences()).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                PengaturanActivity.this.finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}