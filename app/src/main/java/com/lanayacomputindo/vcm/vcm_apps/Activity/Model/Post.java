package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Post {
    private Integer id;
    private Integer branch_id;
    private Integer user_id;
    private String type;
    private String title;
    private String content;
    private String start_date;
    private String end_date;
    private Boolean broadcast;
    private String broadcast_type;
    private String color;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private User user;
    private Branch branch;
    private List<Asset> asset = new ArrayList<Asset>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The branch_id
     */
    public Integer getBranch_id() {
        return branch_id;
    }

    /**
     *
     * @param branch_id
     * The branch_id
     */
    public void setBranch_id(Integer branch_id) {
        this.branch_id = branch_id;
    }

    /**
     *
     * @return
     * The user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     *
     * @param user_id
     * The user_id
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The content
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     * The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @return
     * The start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     *
     * @param start_date
     * The start_date
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     *
     * @return
     * The end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     *
     * @param end_date
     * The end_date
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    /**
     *
     * @return
     * The broadcast
     */
    public Boolean getBroadcast() {
        return broadcast;
    }

    /**
     *
     * @param broadcast
     * The broadcast
     */
    public void setBroadcast(Boolean broadcast) {
        this.broadcast = broadcast;
    }

    /**
     *
     * @return
     * The broadcast_type
     */
    public String getBroadcast_type() {
        return broadcast_type;
    }

    /**
     *
     * @param broadcast_type
     * The broadcast_type
     */
    public void setBroadcast_type(String broadcast_type) {
        this.broadcast_type = broadcast_type;
    }

    /**
     *
     * @return
     * The color
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The branch
     */
    public Branch getBranch() {
        return branch;
    }

    /**
     *
     * @param branch
     * The branch
     */
    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    /**
     *
     * @return
     * The asset
     */
    public List<Asset> getAsset() {
        return asset;
    }

    /**
     *
     * @param asset
     * The asset
     */
    public void setAsset(List<Asset> asset) {
        this.asset = asset;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
