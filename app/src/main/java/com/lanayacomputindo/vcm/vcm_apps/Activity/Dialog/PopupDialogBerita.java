package com.lanayacomputindo.vcm.vcm_apps.Activity.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilNewsActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListPengumumanKelasAdapter;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

public class PopupDialogBerita extends Activity {

    private String bundleid, bundlejudul, bundleisi, bundlecreate, bundlephoto, bundlemerchant;

    private AlertDialog notifdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_notification_dialog);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("idnews");
        bundlejudul = b.getString("judul");
        bundleisi = b.getString("isi");
        bundlecreate = b.getString("create");
        bundlephoto = b.getString("photo");
        bundlemerchant = b.getString("merchant");


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialognotif, null);
        dialogBuilder.setView(dialogView);

        ImageView img = (ImageView) dialogView.findViewById(R.id.image);
        TextView txtjudul = (TextView) dialogView.findViewById(R.id.judul);
        TextView txtdeskripsi = (TextView) dialogView.findViewById(R.id.deskripsi);
        TextView txtparam = (TextView) dialogView.findViewById(R.id.param);
        Button btnTutup = (Button) dialogView.findViewById(R.id.btntutup);
        Button btnLihat = (Button) dialogView.findViewById(R.id.btnlihat);

        Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto.replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(img);

        txtjudul.setText(bundlejudul);
        txtdeskripsi.setText(bundleisi);
        txtparam.setText(getString(R.string.kabar)+" Terbaru");

        btnLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), DetilNewsActivity.class);

                Bundle b = new Bundle();
                b.putString("idnews", bundleid);
                b.putString("judul", bundlejudul);
                b.putString("isi", bundleisi);
                b.putString("create", bundlecreate);
                b.putString("photo", bundlephoto);
                b.putString("merchant", bundlemerchant);

                intent.putExtras(b);

                startActivity(intent);

                finish();
            }
        });

        btnTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        notifdialog = dialogBuilder.create();
        notifdialog.setCancelable(false);
        notifdialog.show();

    }


}
