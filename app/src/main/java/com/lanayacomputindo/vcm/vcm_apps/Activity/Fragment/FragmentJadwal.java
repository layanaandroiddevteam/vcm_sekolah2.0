package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentJadwal extends Fragment {

    public int flagkoneksi = 0;

    View v;

    Bundle b;

    FragmentJadwalHarian frag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_guru, container, false);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        ViewPager viewPager = (ViewPager) v.findViewById(R.id.tabanim_viewpager);

        frag = new FragmentJadwalHarian();
        b = new Bundle();
        b.putString("hari", "monday");
        frag.setArguments(b);
        adapter.addFrag(frag, "Senin");

        frag = new FragmentJadwalHarian();
        b = new Bundle();
        b.putString("hari", "tuesday");
        frag.setArguments(b);
        adapter.addFrag(frag, "Selasa");


        frag = new FragmentJadwalHarian();
        b = new Bundle();
        b.putString("hari", "wednesday");
        frag.setArguments(b);
        adapter.addFrag(frag, "Rabu");



        frag = new FragmentJadwalHarian();
        b = new Bundle();
        b.putString("hari", "thursday");
        frag.setArguments(b);
        adapter.addFrag(frag, "Kamis");


        frag = new FragmentJadwalHarian();
        b = new Bundle();
        b.putString("hari", "friday");
        frag.setArguments(b);
        adapter.addFrag(frag, "Jumat");


        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(5);
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabanim_tabs);
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return mFragmentTitleList.get(position);
            //return null;
        }
    }

}