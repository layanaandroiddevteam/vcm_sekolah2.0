package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentDaftarGuruOnline;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentSurvey;
import com.lanayacomputindo.vcm.vcm_apps.R;

public class DaftarGuruOnlineActivity extends AppCompatActivity {

    Fragment fragment = null;
    Class fragmentClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_guru_online);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Pilih Guru");

        Bundle b = getIntent().getExtras();
        String bundleid = b.getString("id_subject");
        String bundlename = b.getString("name_subject");
        String bundleclass = b.getString("class");

        Log.e("bundle", bundleid + bundlename);

        b.putString("id_subject",bundleid);
        b.putString("name_subject",bundlename);
        b.putString("class",bundleclass);
        fragmentClass = FragmentDaftarGuruOnline.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.nav_contentframe, fragment).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
