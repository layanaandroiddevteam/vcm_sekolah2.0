package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clover_studio.spikachatmodule.utils.Const;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Subject;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Teach;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import com.clover_studio.spikachatmodule.models.User;
import com.clover_studio.spikachatmodule.ChatActivity;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListTeachAdapter extends RecyclerView.Adapter<ListTeachAdapter.ViewHolder> {

    List<Teach> listTeach;
    private String classs;
    private Subject subject;
    private Context context;

    public ListTeachAdapter(Context context, ArrayList<Teach> list, Subject subject, String classs) {
        super();

        this.context = context;
        this.subject = subject;
        this.classs = classs;
        listTeach = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.teach_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Teach modelteach = listTeach.get(i);
        viewHolder.txtnama.setText(modelteach.getUser().getName());
        Picasso.with(context).load(BuildConfig.BASE_API_URL + "/" + modelteach.getUser().getPhoto().replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(viewHolder.gambar_card);
        //getdata
        viewHolder.currentItem = listTeach.get(i);

    }



    @Override
    public int getItemCount() {
        return listTeach.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtnama, txtstatus;

        public Teach currentItem;

        public ImageView gambar_card;

        public ViewHolder(View itemView) {
            super(itemView);
            txtnama = (TextView)itemView.findViewById(R.id.txtnama);
            txtstatus = (TextView)itemView.findViewById(R.id.txtstatus);
            gambar_card = (ImageView)itemView.findViewById(R.id.gambar_card);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("shared",
                            Activity.MODE_PRIVATE);
                    int id = sharedPreferences.getInt("id", 0);
                    String gcm_server  = sharedPreferences.getString("gcm_server", "");
                    String token  = sharedPreferences.getString("token", "");
                    String name = sharedPreferences.getString("name", "");
                    String photo = sharedPreferences.getString("photo", "");
                    User user = new User();
                    user.userID = String.valueOf(id);
                    user.name = name;
                    user.avatarURL = BuildConfig.BASE_API_URL + "/" + photo.replace("\\", "/");
                    user.roomID = "X"+String.valueOf(id)+"X-X"+String.valueOf(currentItem.getUser().getId())+"X-X"+subject.getId()+"X";
                    user.roomName = name;
                    user.subjectID = String.valueOf(subject.getId());
                    user.subjectName = subject.getName();
                    user.className = classs;
                    user.gcm = token;
                    user.gcmServer = gcm_server;
                    Log.d("roomid",user.roomID + gcm_server);
                    ChatActivity.startChatActivity(context, user, currentItem.getUser().getName());
                }
            });
        }
    }
}