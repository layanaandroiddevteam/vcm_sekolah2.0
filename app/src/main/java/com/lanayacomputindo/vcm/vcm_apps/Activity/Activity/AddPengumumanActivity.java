package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.ClassRoom;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.PersonUser;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.User;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPengumumanActivity extends AppCompatActivity {

    private Button btnPilih, btnkirim;

    private EditText edtitle, edisi;

    private SearchableSpinner spKelas, spWali;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private RadioGroup radiojenis;
    private RadioButton radiopilih;

    LinearLayout llclass, llprivate;

    Bitmap imageupload = null;

    private ProgressDialog pDialog;

    File fileimage = null;

    ImageView imgphoto;

    String[] permissioncamera = new String[]{Manifest.permission.CAMERA};

    private String broadcast = "all", class_room_id = "", parent = "";

    private RestClient.GitApiInterface service;

    private ArrayList<ClassRoom> listKelas = new ArrayList<ClassRoom>();
    private ArrayList<String> listNamakelas = new ArrayList<String>();
    private ArrayAdapter<String> adapterKelas;

    private ArrayList<User> listWali = new ArrayList<User>();
    private ArrayList<String> listNamaWali = new ArrayList<String>();
    private ArrayAdapter<String> adapterWali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pengumuman);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Tambah Pengumuman");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        service = RestClient.getClient(this);

        edtitle = (EditText) findViewById(R.id.title);
        edisi = (EditText) findViewById(R.id.isi);
        btnPilih = (Button) findViewById(R.id.btnPilih);
        btnkirim = (Button) findViewById(R.id.btnkirim);

        imgphoto = (ImageView) findViewById(R.id.photo);

        spKelas = (SearchableSpinner) findViewById(R.id.spkelas);
        spWali = (SearchableSpinner) findViewById(R.id.spwalimurid);

        spKelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (listKelas.size()>0) {
                    class_room_id = String.valueOf(listKelas.get(i).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spWali.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (listWali.size()>0) {
                    parent = String.valueOf(listWali.get(i).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnkirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageupload!=null){
                    if(broadcast.equalsIgnoreCase("all")) {
                        postPengumuman();
                    }
                    if(broadcast.equalsIgnoreCase("class")) {
                        if(!class_room_id.equalsIgnoreCase("")) postPengumuman();
                    }
                    if(broadcast.equalsIgnoreCase("private")) {
                        if(!parent.equalsIgnoreCase("")) postPengumuman();
                    }
                }
            }
        });

        btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckPermission
                        .from(AddPengumumanActivity.this)
                        .setPermissions(permissioncamera)
                        .setRationaleConfirmText("Meminta ijin mengakses kamera")
                        .setDeniedMsg("The Camera Denied")
                        .setPermissionListener(new PermissionListener() {

                            @Override
                            public void permissionGranted() {
                                if(Build.VERSION.SDK_INT == 23) {
                                    int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                                    if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                        selectImage();
                                    }
                                }
                                selectImage();
                            }

                            @Override
                            public void permissionDenied() {
                                Toast.makeText(getApplicationContext(), "Mengakses kamera tidak diijinkan", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .check();
            }
        });

        llclass = (LinearLayout) findViewById(R.id.llkelas);
        llprivate = (LinearLayout) findViewById(R.id.llwali);
        llclass.setVisibility(View.GONE);
        llprivate.setVisibility(View.GONE);

        radiojenis = (RadioGroup) findViewById(R.id.rg);
        radiojenis.check(R.id.rb_all);

        int selectedId = radiojenis.getCheckedRadioButtonId();

        radiopilih = (RadioButton) findViewById(selectedId);

        radiojenis.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectId = radiojenis.getCheckedRadioButtonId();

                radiopilih = (RadioButton) findViewById(selectId);

                if(radiopilih.getText().toString().equalsIgnoreCase("Semua"))
                {
                    broadcast = "all";
                    llclass.setVisibility(View.GONE);
                    llprivate.setVisibility(View.GONE);
                }
                else if(radiopilih.getText().toString().equalsIgnoreCase("Kelas"))
                {
                    broadcast = "class";
                    llclass.setVisibility(View.VISIBLE);
                    llprivate.setVisibility(View.GONE);
                }
                else if(radiopilih.getText().toString().equalsIgnoreCase("Walimurid"))
                {
                    broadcast = "private";
                    llclass.setVisibility(View.GONE);
                    llprivate.setVisibility(View.VISIBLE);
                }
            }
        });


        getKelas();

    }

    private void selectImage() {
        final CharSequence[] items = { "Ambil Foto", "Pilih dari Gallery",
                "Batal" };

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPengumumanActivity.this);
        builder.setTitle("Tambah Gambar!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {

                    fileimage = new File(Environment.getExternalStorageDirectory(),
                            "kartuvirtual.jpg");

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileimage));
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }


                } else if (items[item].equals("Pilih dari Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (requestCode == SELECT_FILE) {
                imageUri = CropImage.getPickImageResultUri(this, data);
            }
            else if (requestCode == REQUEST_CAMERA) {
                imageUri = Uri.fromFile(fileimage);
                //imageUri = CropImage.getPickImageResultUri(this, data);
            }

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imgphoto.setImageURI(result.getUri());

                Bitmap bitmap = ((BitmapDrawable)imgphoto.getDrawable()).getBitmap();

                imageupload = bitmap;

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void getKelas()
    {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        pDialog = ProgressDialog.show(AddPengumumanActivity.this,
                "",
                "Tunggu Sebentar!");

        Call<Results<ClassRoom>> call = service.getClassBranch("branch");
        call.enqueue(new Callback<Results<ClassRoom>>() {
            @Override
            public void onResponse(Call<Results<ClassRoom>> call, Response<Results<ClassRoom>> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        listKelas.clear();
                        listNamakelas.clear();

                        for (final ClassRoom classRoom : response.body().getData()) {
                            listKelas.add(classRoom);
                            listNamakelas.add(classRoom.getName());
                        }
                        adapterKelas = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.spinner_item, listNamakelas);

                        spKelas.setAdapter(adapterKelas);
                        getWaliMurid();
                    } else {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        Log.e("Get Kelas" + " response", String.valueOf(response.raw().toString()));
                    }
                } else {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    Log.e("Get Kelas" + " response", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Results<ClassRoom>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                Log.e("Get Kelas" + " failure", String.valueOf(t.getMessage().toString()));
            }
        });

    }

    public void getWaliMurid()
    {

        Call<Results<PersonUser>> call = service.getWaliMurid("user");
        call.enqueue(new Callback<Results<PersonUser>>() {
            @Override
            public void onResponse(Call<Results<PersonUser>> call, Response<Results<PersonUser>> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        listWali.clear();
                        listNamaWali.clear();

                        for (final PersonUser user : response.body().getData()) {
                            listWali.add(user.getUser());
                            listNamaWali.add(user.getUser().getName());
                        }
                        adapterWali = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.spinner_item, listNamaWali);

                        spWali.setAdapter(adapterWali);
                    } else {
                        Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        Log.e("Get Wali" + " response", String.valueOf(response.raw().toString()));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    Log.e("Get Wali" + " response", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Results<PersonUser>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                Log.e("Get Wali" + " failure", String.valueOf(t.getMessage().toString()));
            }
        });

    }

    public void postPengumuman(){
        pDialog = ProgressDialog.show(AddPengumumanActivity.this,
                "Mengirim Pengumuman",
                "Tunggu Sebentar!");

        String image = getStringImage(imageupload);
        RestClient.GitApiInterface service = RestClient.getClient(this);

        Log.d("isi post", edtitle.getText().toString() + edisi.getText().toString() + broadcast + class_room_id + parent);
        Call<Result<Post>> call = service.postPengumuman("news",edtitle.getText().toString(), edisi.getText().toString(), true, broadcast, image, class_room_id, parent);

        call.enqueue(new Callback<Result<Post>>() {
            @Override
            public void onResponse(Call<Result<Post>> call, Response<Result<Post>> response) {
                Log.d("Post", "Status Code = " + response.code());
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    Result<Post> result = response.body();
                    Log.d("Post", "response = " + new Gson().toJson(result));
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    finish();

                } else {
                    Log.e("Post", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Result<Post>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("Post", t.toString());
                Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
