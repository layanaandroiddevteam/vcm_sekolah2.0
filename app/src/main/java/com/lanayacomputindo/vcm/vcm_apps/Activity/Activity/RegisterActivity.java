package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.User;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    protected static final int DATE_DIALOG_ID = 0;

    private ImageButton btndaftar;

    private TextView cagree;

    public static Activity ActRegister;
    private ProgressDialog pDialog;

    private SharedPreferences sharedPreferences;

    private EditText ednama, edemail, edtelp, edalamat, edpassword, edlahir;

    private BetterSpinner spagama;

    private CheckBox cek;

    String nama, telp, alamat, email, password, tgl_lahir, agama, tanggal;

    boolean isverify;

    private void savePreferences(String email,
                                 String password) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("emaildaftar", email);
        editor.putString("passworddaftar", password);
        editor.putString("namadaftar", nama);
        editor.commit();
    }


    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            isverify = sharedPreferences.getBoolean("isverify", false);
        } else {
            isverify = false;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        loadPreferences();

        ActRegister = this;

        if(isverify)
        {
            Intent i = new Intent(getApplicationContext(), VerifikasiActivity.class);
            startActivity(i);
            finish();
        }
        else {
            btndaftar = (ImageButton) findViewById(R.id.btndaftar);

            cek = (CheckBox) findViewById(R.id.checkBox);
            cek.setChecked(true);

            cagree = (TextView) findViewById(R.id.cagree);

            cagree.setClickable(true);
            cagree.setMovementMethod(LinkMovementMethod.getInstance());
            String text = "Saya setuju dengan <a style='color:#4EA30D' href=' http://kartuvirtual.com'>persetujuan</a> dan <a href=' http://kartuvirtual.com'>kebijakan</a> dari pengembang dan pengembang aplikasi";
            cagree.setText(Html.fromHtml(text));

            ednama = (EditText) findViewById(R.id.nama);
            edalamat = (EditText) findViewById(R.id.alamat);
            edemail = (EditText) findViewById(R.id.email);
            edtelp = (EditText) findViewById(R.id.telp);
            edpassword = (EditText) findViewById(R.id.password);
            edlahir = (EditText) findViewById(R.id.lahir);
            spagama = (BetterSpinner) findViewById(R.id.agama);
            String[] list = getResources().getStringArray(R.array.spinneragama);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_dropdown_item_1line, list);

            spagama.setAdapter(adapter);

            edlahir.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // TODO Auto-generated method stub
                    if (v == edlahir)
                        showDialog(DATE_DIALOG_ID);
                    return false;
                }
            });

            btndaftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    cekRegister();

                }
            });
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private void cekRegister()
    {
        boolean cancel = true;

        nama = ednama.getText().toString();
        telp = edtelp.getText().toString();
        alamat = edalamat.getText().toString();
        email = edemail.getText().toString();
        password = edpassword.getText().toString();
        tgl_lahir = tanggal;
        agama = spagama.getText().toString();

        Log.d("agama", agama);

        if (email.equalsIgnoreCase("")||alamat.equalsIgnoreCase("")||telp.equalsIgnoreCase("")||nama.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(),"Data tidak boleh ada yang kosong",Toast.LENGTH_LONG).show();
            cancel = false;
        } else if (!isEmailValid(email)) {
            Toast.makeText(getApplicationContext(),R.string.invalidemail,Toast.LENGTH_LONG).show();
            cancel = false;
        }

        if (cancel) {

            if(cek.isChecked()) {
                pDialog = ProgressDialog.show(RegisterActivity.this,
                        "Mendaftar",
                        "Tunggu Sebentar!");

                register();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Anda harus menyetujui persetujuan yang kami berikan",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void register()
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.register(BuildConfig.CLIENT_ID, email, password, nama, telp, alamat, tgl_lahir, agama, "parent");
        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call,Response<Result<User>> response) {
                Log.d("Register", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Register", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        savePreferences(email, password);

                        Intent i = new Intent(getApplicationContext(), VerifikasiActivity.class);
                        startActivity(i);
                        pDialog.dismiss();
                        finish();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Email anda sudah terdaftar", Toast.LENGTH_LONG).show();
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.e("Login", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();

                }
            }

            @Override
            public void onFailure(Call<Result<User>> call, Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Log.d("gagal", "agagagagal");
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, cyear, cmonth, cday);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            tanggal = date_selected;
            edlahir.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+String.valueOf(year));
        }
    };

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

}
