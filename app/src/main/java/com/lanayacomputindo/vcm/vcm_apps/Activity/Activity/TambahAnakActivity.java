package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.TimeFormater;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.ClassRoom;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahAnakActivity extends AppCompatActivity {

    protected static final String TAG = "TambahAnak";
    protected static final int DATE_DIALOG_ID = 0;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private RestClient.GitApiInterface service;

    private String id,  nama,  email,  telp,  alamat,  photo,  status,  qrbarcode,  imageqr, tgl_lahir, agama, tanggal="";
    private int branchId = 0;
    private int classId = 0;

    Bitmap imageupload = null;

    private  ProgressDialog pDialog;

    ImageView imgphoto;

    String[] permissioncamera = new String[]{Manifest.permission.CAMERA};

    private ArrayList<Branch> listSekolah;
    private ArrayList<String> listNamaSekolah;
    private ArrayList<ClassRoom> listKelas;
    private ArrayList<String> listNamakelas;
    private ArrayAdapter<String> adapterKelas;
    private ArrayAdapter<String> adapterSekolah;

    private Bundle b;
    private String operation = "create";
    private String bundlekelasid, bundlecabangid, bundleid,bundlenama,bundlenis, bundlephoto, bundlenamamember, bundlestatus, bundlekelas, bundletgl_lahir, bundlenamamerchant, bundletgl_create;

    private SharedPreferences sharedPreferences;

    File fileimage = null;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            id = sharedPreferences.getString("id_member", "");
            nama = sharedPreferences.getString("nama_member", "");
            email = sharedPreferences.getString("email", "");
            telp = sharedPreferences.getString("telp", "");
            alamat = sharedPreferences.getString("alamat", "");
            photo = sharedPreferences.getString("photo", "");
            status = sharedPreferences.getString("status", "");
            qrbarcode = sharedPreferences.getString("qrbarcode", "");
            imageqr = sharedPreferences.getString("imageqr", "");
            tgl_lahir = sharedPreferences.getString("tgl_lahir", "");
            agama = sharedPreferences.getString("agama", "");
        } else {

        }
    }

    EditText ednama, ednis, edlahir;
    BetterSpinner sekolah, kelas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_anak);

        b = getIntent().getExtras();
        if (b!=null) {
            operation = "update";
            bundleid = b.getString("id_anak");
            bundlenama = b.getString("nama_anak");
            bundlenis = b.getString("nis");
            bundlephoto = b.getString("photo");
            bundlenamamember = b.getString("nama_member");
            bundletgl_lahir = b.getString("tgl_lahir");
            bundlestatus = b.getString("status");
            bundlecabangid = b.getString("cabang_id");
            bundlekelasid = b.getString("kelas_id");
            bundlekelas = b.getString("nama_kelas");
            bundlenamamerchant = b.getString("nama_merchant");
            bundletgl_create = b.getString("tgl_create");

        }

        service = RestClient.getClient(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        listSekolah = new ArrayList<Branch>();
        listNamaSekolah = new ArrayList<String>();
        listKelas = new ArrayList<ClassRoom>();
        listNamakelas = new ArrayList<String>();
        adapterSekolah = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, listNamaSekolah);

        ednama = (EditText) findViewById(R.id.nama);
        ednis = (EditText) findViewById(R.id.nis);
        sekolah = (BetterSpinner) findViewById(R.id.sekolah);
        kelas = (BetterSpinner) findViewById(R.id.kelas);
        edlahir = (EditText) findViewById(R.id.tanggal);
        imgphoto = (ImageView) findViewById(R.id.photo);

        sekolah.setAdapter(adapterSekolah);
        kelas.setAdapter(adapterKelas);

        if (operation.equals("update")) {
            ednama.setText(bundlenama);
            ednis.setText(bundlenis);
            //edlahir.setText(bundletgl_lahir);
            edlahir.setText(new TimeFormater().formattedDateFromString("yyyy-MM-dd", "dd MMMM yyyy", bundletgl_lahir));
            Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto)
                    .error(R.drawable.loadimage)
                    .placeholder(R.drawable.loadimage)
                    .into(imgphoto);
        }

        edlahir.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (v == edlahir)
                    showDialog(DATE_DIALOG_ID);
                return false;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckPermission
                        .from(TambahAnakActivity.this)
                        .setPermissions(permissioncamera)
                        .setRationaleConfirmText("Meminta ijin mengakses kamera")
                        .setDeniedMsg("The Camera Denied")
                        .setPermissionListener(new PermissionListener() {

                            @Override
                            public void permissionGranted() {
                                if(Build.VERSION.SDK_INT == 23) {
                                    int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                                    if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                        selectImage();
                                    }
                                }
                                selectImage();
                            }

                            @Override
                            public void permissionDenied() {
                                Toast.makeText(getApplicationContext(), "Mengakses kamera tidak diijinkan", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .check();

            }
        });

        getSekolah();

        sekolah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listSekolah.size()>0) {
                    Branch branch = listSekolah.get(position);
                    branchId = branch.getId();
                    Log.e(TAG+" branch", String.valueOf(branchId));
                    getKelas(branch.getId());
                }
            }
        });

        kelas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, sekolah.getText().toString());
                if (sekolah.getText() != null || !sekolah.getText().toString().equals("")) {
                    ClassRoom classRoom = listKelas.get(position);
                    classId = classRoom.getId();
                } else {
                    Toast.makeText(getApplicationContext(), "Anda harus memilih sekolah", Toast.LENGTH_SHORT).show();
                }
            }
        });
        adapterKelas = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, listNamakelas);
        listKelas.clear();
        listNamakelas.clear();
        kelas.setAdapter(adapterKelas);
        kelas.setEnabled(false);
        kelas.setHint("Pilih Sekolah Terlebih Dahulu");
    }

    private void getSekolah() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        pDialog = ProgressDialog.show(TambahAnakActivity.this,
                "Mengambil data Sekolah yang tersedia",
                "Tunggu Sebentar!");

        Call<Results<Branch>> call = service.getClientBranch();

        call.enqueue(new Callback<Results<Branch>>() {
            @Override
            public void onResponse(Call<Results<Branch>> call, Response<Results<Branch>> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        for (final Branch branch : response.body().getData()) {
                            listSekolah.add(branch);
                            listNamaSekolah.add(branch.getName());
                            if (operation.equals("update")) {
                                if(Integer.parseInt(bundlecabangid) == branch.getId()) {
                                    branchId = branch.getId();
                                    sekolah.setText(branch.getName());
                                }
                            }
                            Log.e(TAG, branch.getName());
                            if (operation.equals("update")) {
                                getKelas(Integer.valueOf(bundlecabangid));
                            }
                        }
                        adapterSekolah.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Results<Branch>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                Log.e(TAG + " failure", String.valueOf(t.getMessage().toString()));
            }
        });
    }

    public void getKelas(final int sekolahId)
    {
        Log.e("cabang",String.valueOf(sekolahId));
        String search = "branch_id:" + String.valueOf(sekolahId);
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        pDialog = ProgressDialog.show(TambahAnakActivity.this,
                "Mengambil data Kelas yang tersedia",
                "Tunggu Sebentar!");
        Call<Results<ClassRoom>> call = service.getClassRoom(search);
        call.enqueue(new Callback<Results<ClassRoom>>() {
            @Override
            public void onResponse(Call<Results<ClassRoom>> call, Response<Results<ClassRoom>> response) {
                pDialog.dismiss();
                String kelasName = "";
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        if (response.body().getData().size()>0) {
                            kelas.setHint("Pilihan kelas");
                            kelas.setEnabled(true);
                            listKelas.clear();
                            listNamakelas.clear();
                            adapterKelas = new ArrayAdapter<String>(getApplicationContext(),
                                    R.layout.spinner_item, listNamakelas);
                            for (final ClassRoom classRoom : response.body().getData()) {
                                //Log.e("kelasku", bundlekelasid);
                                //Log.e("daftar kelas", String.valueOf(classRoom.getId()));
                                listKelas.add(classRoom);
                                listNamakelas.add(classRoom.getName());
                                if (operation.equals("update")) {
                                    if (Integer.parseInt(bundlekelasid) == classRoom.getId()) {
                                        classId = classRoom.getId();
                                        kelasName = classRoom.getName();
                                    }
                                }
                            }
                            kelas.setAdapter(adapterKelas);
                        } else {
                            adapterKelas = new ArrayAdapter<String>(getApplicationContext(),
                                    R.layout.spinner_item, listNamakelas);
                            listKelas.clear();
                            listNamakelas.clear();
                            kelas.setAdapter(adapterKelas);
                            kelas.setEnabled(false);
                            kelas.setHint("Belum memiliki kelas");
                        }
                        kelas.setText(kelasName);
                    } else {
                        Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Results<ClassRoom>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                Log.e(TAG + " failure", String.valueOf(t.getMessage().toString()));
            }
        });

    }


    private void selectImage() {
        final CharSequence[] items = { "Ambil Foto", "Pilih dari Gallery",
                "Batal" };

        AlertDialog.Builder builder = new AlertDialog.Builder(TambahAnakActivity.this);
        builder.setTitle("Tambah Gambar!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {

                    fileimage = new File(Environment.getExternalStorageDirectory(),
                            "kartuvirtual.jpg");

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileimage));
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }


                } else if (items[item].equals("Pilih dari Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (requestCode == SELECT_FILE) {
                imageUri = CropImage.getPickImageResultUri(this, data);
            }
            else if (requestCode == REQUEST_CAMERA) {
                imageUri = Uri.fromFile(fileimage);
                //imageUri = CropImage.getPickImageResultUri(this, data);
            }

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imgphoto.setImageURI(result.getUri());

                Bitmap bitmap = ((BitmapDrawable)imgphoto.getDrawable()).getBitmap();

                imageupload = bitmap;

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void editProfile() {

        if (isFormCompleted()) {
            pDialog = ProgressDialog.show(TambahAnakActivity.this,
                    "Menyimpan Data Anak Anda",
                    "Tunggu Sebentar!");

            if (imageupload==null) {
                Bitmap bitmap = ((BitmapDrawable) imgphoto.getDrawable()).getBitmap();
                imageupload = bitmap;
            }
            String image = getStringImage(imageupload);
            tanggal = new TimeFormater().formattedDateFromString("dd MMMM yyyy", "yyyy-MM-dd", edlahir.getText().toString());
            if (operation.equals("create")) {
                Call<Result<Person>> call = service.addChild(ednama.getText().toString(), ednis.getText().toString(), tanggal, image, branchId, classId);
                call.enqueue(new Callback<Result<Person>>() {
                    @Override
                    public void onResponse(Call<Result<Person>> call, Response<Result<Person>> response) {
                        pDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getSuccess()) {
                                MainActivity.checkBranch();
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                                Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                            Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                        }
                    }

                    @Override
                    public void onFailure(Call<Result<Person>> call, Throwable t) {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " failure", String.valueOf(t.getMessage().toString()));
                    }
                });
            } else {
                int personId = Integer.parseInt(bundleid);
                Call<Result<Person>> call = service.updateChild(personId, ednama.getText().toString(), ednis.getText().toString(), tanggal, image, branchId, classId);
                call.enqueue(new Callback<Result<Person>>() {
                    @Override
                    public void onResponse(Call<Result<Person>> call, Response<Result<Person>> response) {
                        pDialog.dismiss();
                        if (response.isSuccessful()) {
                            if (response.body().getSuccess()) {
                                resultUpdate(response.body().getData());
                                MainActivity.checkBranch();
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                                Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                            Log.e(TAG + " response", String.valueOf(response.raw().toString()));
                        }
                    }

                    @Override
                    public void onFailure(Call<Result<Person>> call, Throwable t) {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " failure", String.valueOf(t.getMessage().toString()));
                    }
                });
            }
        }

    }

    private void resultUpdate(Person person) {
        Intent i = new Intent();
        Bundle b = new Bundle();
        b.putString("id_anak", String.valueOf(person.getId()));
        b.putString("nama_anak", String.valueOf(person.getName()));
        b.putString("nis", String.valueOf(person.getNip()));
        b.putString("photo", String.valueOf(person.getPhoto()));
        b.putString("tgl_lahir", String.valueOf(person.getBirthday()));
        b.putString("status", String.valueOf(person.getStatus()));
        b.putString("cabang_id", String.valueOf(branchId));
        b.putString("kelas_id", String.valueOf(classId));
        b.putString("nama_kelas", String.valueOf(sekolah.getText().toString() + " - Kelas " + kelas.getText().toString()));
        b.putString("tgl_create", String.valueOf(person.getCreated_at()));
        i.putExtras(b);
        setResult(Activity.RESULT_OK, i);
        finish();
    }

    private boolean isFormCompleted() {
        if (branchId == 0 || classId == 0) {
            Toast.makeText(this, "Sekolah & Kelas tidak boleh kosong", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tambahanak, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                if(MainActivity.checkBranch==1)
                {
                    finish();
                    MainActivity.act.finish();
                }
                else
                {
                    finish();
                }


                return true;

            case R.id.menu_tambah_anak:
                if((ednama.getText().toString().equalsIgnoreCase("")||ednis.getText().toString().equalsIgnoreCase("")||edlahir.getText().toString().equalsIgnoreCase("")||imageupload==null) &&(operation.equals("create")) )
                {
                    Toast.makeText(getApplicationContext(), "Data Anak anda harus lengkap", Toast.LENGTH_SHORT).show();
                }
                else {
                    editProfile();
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, cyear, cmonth, cday);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            tanggal = date_selected;
            edlahir.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+String.valueOf(year));
        }
    };

}
