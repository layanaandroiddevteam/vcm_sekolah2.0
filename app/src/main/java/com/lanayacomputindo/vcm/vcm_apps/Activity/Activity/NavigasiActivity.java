package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListTokoMercantAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.DirectionParser;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.MyApplication;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NavigasiActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {

    private static GoogleMap mMap;

    private final int[] MAP_TYPES = { GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN,
            GoogleMap.MAP_TYPE_NONE };

    static LatLng posisiAwal;
    static int tandaline = 0;
    static int tandamovecamera = 0;
    static String duration="", distance="";

    static int jumlahtoko;
    static int inc = 1;

    LocationManager locManager;
    Location currentLoc;

    static Polyline line;

    private GoogleApiClient googleApiClient;

    static RecyclerView mRecyclerView;
    static RecyclerView.Adapter mAdapter;

    static ArrayList<Branch> listTokoMerchant = new ArrayList<Branch>();
    static ArrayList<String> listDuration = new ArrayList<String>();
    static ArrayList<String> listDistance = new ArrayList<String>();

    Call<Results<Branch>> call;
    RestClient.GitApiInterface service;

    String[] dangerousPermissionLocation = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigasi);
        tandaline = 0;
        tandamovecamera = 0;
        inc = 1;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Navigasi");

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerView.setHasFixedSize(true);

        googleApiClient = new GoogleApiClient.Builder(NavigasiActivity.this)
                .addConnectionCallbacks(NavigasiActivity.this) // connection callback
                .addOnConnectionFailedListener(NavigasiActivity.this) // when connection failed
                .addApi(LocationServices.API) // called api
                .build();

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        CheckPermission
                .from(NavigasiActivity.this)
                .setPermissions(dangerousPermissionLocation)
                .setRationaleConfirmText("Meminta ijin deteksi likasi anda")
                .setDeniedMsg("The Location Denied")
                .setPermissionListener(new PermissionListener() {

                    @Override
                    public void permissionGranted() {
                        googleApiClient.connect();
                    }

                    @Override
                    public void permissionDenied() {
                        Toast.makeText(getApplicationContext(), "Lokasi tidak diijinkan", Toast.LENGTH_SHORT).show();
                    }
                })
                .check();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        setTokoInMap();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void setTokoInMap()
    {
        try {

            if(netCheckin()) {

                locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

                if ( !locManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }
                else {

                    currentLoc = LocationServices.FusedLocationApi.getLastLocation( googleApiClient );

                    posisiAwal = new LatLng(currentLoc.getLatitude(), currentLoc.getLongitude());

                    mMap.clear();
                    mMap.setMapType(MAP_TYPES[1]);
                    mMap.setTrafficEnabled(true);
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setZoomControlsEnabled(true);

                    service = RestClient.getClient(this);
                    call = service.getClientBranch();

                    call.enqueue(new Callback<Results<Branch>>() {
                        @Override
                        public void onResponse(Call<Results<Branch>> call,  Response<Results<Branch>> response) {
                            Log.d("MainActivity", "Status Code = " + response.code());
                            if (response.isSuccessful()) {
                                // request successful (status code 200, 201)
                                Results<Branch> result = response.body();
                                Log.d("MainActivity", "response = " + new Gson().toJson(result));
                                listTokoMerchant = result.getData();
                                jumlahtoko = listTokoMerchant.size();
                                for (int i = 0; i < listTokoMerchant.size(); i++) {
                                    if(!listTokoMerchant.get(i).getLatitude().equalsIgnoreCase("")&&!listTokoMerchant.get(i).getLongitude().equalsIgnoreCase("")) {
                                        LatLng posisitoko = new LatLng(Double.parseDouble(listTokoMerchant.get(i).getLatitude()), Double.parseDouble(listTokoMerchant.get(i).getLongitude()));
                                        mMap.addMarker(new MarkerOptions().position(posisitoko).title(listTokoMerchant.get(i).getName()).snippet(listTokoMerchant.get(i).getAddress()).icon(BitmapDescriptorFactory.fromResource(R.drawable.map)));

                                        Log.d("TextLokasi", listTokoMerchant.get(i).getName());

                                        LatLng origin = posisiAwal;
                                        LatLng dest = posisitoko;

                                        // Getting URL to the Google Directions API
                                        String url = getDirectionsUrl(origin, dest);

                                        DownloadTask downloadTask = new DownloadTask();

                                        // Start downloading json data from Google Directions API
                                        downloadTask.execute(url);
                                    }
                                    else
                                    {
                                        Toast.makeText(NavigasiActivity.this, "Sekolah tidak memiliki lokasi", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {
                                // response received but request not successful (like 400,401,403 etc)
                                //Handle errors
                                Log.e("on Failure", "asas");

                            }
                        }

                        @Override
                        public void onFailure(Call<Results<Branch>> call,Throwable t) {
                            Log.d("MainActivity", "gagalall");
                            Log.e("on Failure", t.toString());
                            Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
                        }
                    });


                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posisiAwal, 13));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);


                    //mMap.setOnMarkerClickListener(this);
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Cek kembali koneksi anda", Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Toast.makeText(getApplicationContext(),"Tunggu beberapa saat lagi",Toast.LENGTH_LONG).show();
        }
    }

    public static void moveCameraMap(LatLng location)
    {
        tandamovecamera = 1;

        if(tandaline == 1) {
            line.remove();
        }

        int zoom = (int)mMap.getCameraPosition().zoom;
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(location, zoom);
        mMap.animateCamera(cu);

        LatLng origin = posisiAwal;
        LatLng dest = location;

        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }

    private boolean netCheckin() {
        try {
            ConnectivityManager nInfo = (ConnectivityManager) getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            nInfo.getActiveNetworkInfo().isConnectedOrConnecting();
            Log.d("", "Net avail:"
                    + nInfo.getActiveNetworkInfo().isConnectedOrConnecting());
            ConnectivityManager cm = (ConnectivityManager) getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                Log.d("", "Network available:true");
                return true;
            } else {
                Log.d("", "Network available:false");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("GPS anda tidak aktif, aktifkan sekarang?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }






    private static String getDirectionsUrl(LatLng origin,LatLng dest){

        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private static String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception while", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private static class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private static class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionParser parser = new DirectionParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            if(result.size()<1){
                //Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){    // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.RED);
            }

            Log.d("ayok", "Distance:" + distance + ", Duration:"+duration);

            // Drawing polyline in the Google Map for the i-th route
            if(tandamovecamera==1) {
                line = mMap.addPolyline(lineOptions);
                tandaline = 1;
            }
            else
            {
                listDistance.add(distance);
                listDuration.add(duration);
                if(inc == jumlahtoko)
                {
                    mAdapter = new ListTokoMercantAdapter(MyApplication.getAppContext(), listTokoMerchant, listDuration, listDistance);
                    mRecyclerView.setAdapter(mAdapter);
                }
                inc++;
            }
        }
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                NavigasiActivity.this.finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
