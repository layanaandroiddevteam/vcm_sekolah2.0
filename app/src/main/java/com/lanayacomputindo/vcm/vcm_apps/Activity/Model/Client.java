package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Client {
    private Integer id;
    private String type;
    private Integer role_id;
    private String name;
    private String owner;
    private String photo;
    private Integer reg_number;
    private String phone;
    private String address;
    private String decscription;
    private String whatapps;
    private String bbm;
    private String color;
    private String background;
    private String website;
    private String youtube;
    private String api_key;
    private String sender_id;
    private String link;
    private String packet;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Role role;
    private List<User> user = new ArrayList<User>();
    private List<Branch> branch = new ArrayList<Branch>();
    private List<Transaction> transaction = new ArrayList<Transaction>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The role_id
     */
    public Integer getRole_id() {
        return role_id;
    }

    /**
     *
     * @param role_id
     * The role_id
     */
    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     *
     * @param owner
     * The owner
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     *
     * @return
     * The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The reg_number
     */
    public Integer getReg_number() {
        return reg_number;
    }

    /**
     *
     * @param reg_number
     * The reg_number
     */
    public void setReg_number(Integer reg_number) {
        this.reg_number = reg_number;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The decscription
     */
    public String getDecscription() {
        return decscription;
    }

    /**
     *
     * @param decscription
     * The decscription
     */
    public void setDecscription(String decscription) {
        this.decscription = decscription;
    }

    /**
     *
     * @return
     * The whatapps
     */
    public String getWhatapps() {
        return whatapps;
    }

    /**
     *
     * @param whatapps
     * The whatapps
     */
    public void setWhatapps(String whatapps) {
        this.whatapps = whatapps;
    }

    /**
     *
     * @return
     * The bbm
     */
    public String getBbm() {
        return bbm;
    }

    /**
     *
     * @param bbm
     * The bbm
     */
    public void setBbm(String bbm) {
        this.bbm = bbm;
    }

    /**
     *
     * @return
     * The color
     */
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     * The color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     * The background
     */
    public String getBackground() {
        return background;
    }

    /**
     *
     * @param background
     * The background
     */
    public void setBackground(String background) {
        this.background = background;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The youtube
     */
    public String getYoutube() {
        return youtube;
    }

    /**
     *
     * @param youtube
     * The youtube
     */
    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    /**
     *
     * @return
     * The api_key
     */
    public String getApi_key() {
        return api_key;
    }

    /**
     *
     * @param api_key
     * The api_key
     */
    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    /**
     *
     * @return
     * The sender_id
     */
    public String getSender_id() {
        return sender_id;
    }

    /**
     *
     * @param sender_id
     * The sender_id
     */
    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The packet
     */
    public String getPacket() {
        return packet;
    }

    /**
     *
     * @param packet
     * The packet
     */
    public void setPacket(String packet) {
        this.packet = packet;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The role
     */
    public Role getRole() {
        return role;
    }

    /**
     *
     * @param role
     * The role
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     *
     * @return
     * The user
     */
    public List<User> getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(List<User> user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The branch
     */
    public List<Branch> getBranch() {
        return branch;
    }

    /**
     *
     * @param branch
     * The branch
     */
    public void setBranch(List<Branch> branch) {
        this.branch = branch;
    }

    /**
     *
     * @return
     * The transaction
     */
    public List<Transaction> getTransaction() {
        return transaction;
    }

    /**
     *
     * @param transaction
     * The transaction
     */
    public void setTransaction(List<Transaction> transaction) {
        this.transaction = transaction;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
