package com.lanayacomputindo.vcm.vcm_apps.Activity.Helper;

import android.app.Application;
import android.content.Context;

/**
 * Created by Yoshua on 3/23/2016.
 */
public class MyApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}