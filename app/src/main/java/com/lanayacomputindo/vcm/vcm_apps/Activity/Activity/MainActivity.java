package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.clover_studio.spikachatmodule.ChatActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Gcm.QuickstartPreferences;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Gcm.RegistrationIntentService;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.PersonClass;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Update;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static Activity act;
    public static String foto;
    public static int checkBranch=1;

    public static Spinner spbranch;
    public static TextView txtKelas;
    public static ImageView imgAnak;

    private static ProgressDialog pDialog;

    private LinearLayout llNilai, llAbsen, llKabar, llJadwal;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static Toolbar toolbar;

    static int id,client_id;
    static boolean status;
    static String type, name,email,birthday,religion,phone,address,photo,created_at,updated_at;

    private static ArrayList<Integer> listidbranch = new ArrayList<Integer>();
    private static ArrayList<String> listemailbranch = new ArrayList<String>();
    private static ArrayList<Integer> listidanak = new ArrayList<Integer>();
    private static ArrayList<String> listnamaanak = new ArrayList<String>();
    private static ArrayList<String> listkelasanak = new ArrayList<String>();
    private static ArrayList<String> listfotoanak = new ArrayList<String>();

    private static SharedPreferences sharedPreferences;

    private AlertDialog dialog;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            id = sharedPreferences.getInt("id", 0);
            client_id = sharedPreferences.getInt("client_id", 0);
            type = sharedPreferences.getString("type", "");
            name = sharedPreferences.getString("name", "");
            email = sharedPreferences.getString("email", "");
            birthday = sharedPreferences.getString("birthday", "");
            religion = sharedPreferences.getString("religion", "");
            phone = sharedPreferences.getString("phone", "");
            address = sharedPreferences.getString("address", "");
            photo = sharedPreferences.getString("photo", "");
            status = sharedPreferences.getBoolean("status", false);
            created_at = sharedPreferences.getString("created_at", "");
            updated_at = sharedPreferences.getString("updated_at", "");
        } else {

        }
    }

    private void savePreferences(boolean islogin, boolean isverify) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putBoolean("isverify", isverify);
        editor.commit();
    }

    private static void saveBranch(int branch_id, String branch_email, int anak_id, String anak_name, String anak_kelas, String anak_foto) {
        sharedPreferences = act.getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("branch_id", branch_id);
        editor.putString("branch_email", branch_email);
        editor.putInt("anak_id", anak_id);
        editor.putString("anak_name", anak_name);
        editor.putString("anak_kelas", anak_kelas);
        editor.putString("anak_foto", anak_foto);
        editor.commit();
    }

    private void saveToken(String token) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", token);
        editor.commit();
    }

    private void saveServerKey(String gcm_server) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("gcm_server", gcm_server);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        act = this;
        savePreferences(true, false);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        //txtName = (TextView) toolbar.findViewById(R.id.txtName);
        //txtEmail = (TextView) toolbar.findViewById(R.id.txtEmail);
        //imgPhoto = (ImageView) toolbar.findViewById(R.id.imgPhoto);

        txtKelas = (TextView) findViewById(R.id.txtKelas);
        imgAnak = (ImageView) findViewById(R.id.imgAnak);

        llNilai = (LinearLayout) findViewById(R.id.llNilai);
        llNilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FragmentActivity.class);
                i.putExtra("type", "nilai");
                startActivity(i);
            }
        });
        llAbsen = (LinearLayout) findViewById(R.id.llAbsen);
        llAbsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FragmentActivity.class);
                i.putExtra("type", "absen");
                startActivity(i);
            }
        });
        llKabar = (LinearLayout) findViewById(R.id.llKabar);
        llKabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FragmentActivity.class);
                i.putExtra("type", "kabar");
                startActivity(i);
            }
        });
        llJadwal = (LinearLayout) findViewById(R.id.llJadwal);
        llJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), FragmentActivity.class);
                i.putExtra("type", "jadwal");
                startActivity(i);
            }
        });


        spbranch = (Spinner) findViewById(R.id.zzxx);
        spbranch.getBackground().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.warna_utama), PorterDuff.Mode.SRC_ATOP);

        loadPreferences();

        if (checkPlayServices()) {
            //Jika iya, maka akan dipanggil Class RegistrationIntentService.java yang akan meminta Token dari server GCM
            Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
            startService(intent);
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                //Memanggil sharedPreferences yang memiliki informasi apakah token sudah didapatkan atau belum
                //dengan menggunakan getBoolean
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                String token = sharedPreferences.getString(QuickstartPreferences.REGISTRATION_TOKEN, "");

                if (sentToken) {
                    //Jika sentToken bernilai true, maka Token yang sudah didapatkan
                    //akan ditampilkan pada txtInfo dan juga Toast

                    checkDevice(token);
                    saveToken(token);

                } else {
                    Toast.makeText(getApplicationContext(), "Token gagal dibuat.. silahkan cek koneksi dan coba lagi", Toast.LENGTH_LONG).show();
                }
            }
        };

        checkBranch();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.menu_edit_profil:

                Intent i = new Intent(act, EditProfileActivity.class);
                startActivity(i);
                break;

            case R.id.menu_keluar:
                checkDevice("");
                Intent j = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(j);
                finish();
                savePreferences(false, false);
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        loadPreferences();
        foto = photo;
        changeImage();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    public static void changeImage()
    {
        //txtName.setText(name);
        //txtEmail.setText(email);

//
//        String photoo = foto.replace("\\", "/");
//        Picasso.Builder builder = new Picasso.Builder(act);
//        builder.listener(new Picasso.Listener()
//        {
//            @Override
//            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
//            {
//                exception.printStackTrace();
//            }
//        });
//
//        Picasso.with(act).load(BuildConfig.BASE_API_URL + "/" + photoo)
//                .error(R.drawable.loadimage)
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .placeholder(R.drawable.loadimage)
//                .into(imgPhoto);
    }

    public static void getHeader()
    {

        //txtName.setText(name);
        //txtEmail.setText(email);

        String photoo = photo.replace("\\", "/");

//        Picasso.Builder builder = new Picasso.Builder(act);
//        builder.listener(new Picasso.Listener()
//        {
//            @Override
//            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
//            {
//                exception.printStackTrace();
//            }
//        });
//        builder.build().load(BuildConfig.BASE_API_URL + "/" + photoo).into(imgPhoto);


        if(type.equalsIgnoreCase("client")){
            spbranch.setVisibility(View.GONE);
        }
        else{
            ArrayAdapter<String> adapter  = new ArrayAdapter<String>(act,
                    R.layout.spinner_item_white, listnamaanak);
            adapter.setDropDownViewResource(R.layout.spinner_item);
            spbranch.setAdapter(adapter);

            spbranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int j, long l) {
                    int idbranch=0, anak_id = 0;
                    String branch_email = "", anak_name = "", anak_kelas = "",anak_foto = "";
                    for(int i = 0;i<listnamaanak.size();i++)
                    {
                        if(listnamaanak.get(i).equalsIgnoreCase(spbranch.getSelectedItem().toString()))
                        {
                            idbranch = listidbranch.get(i);
                            branch_email = listemailbranch.get(i);
                            anak_id = listidanak.get(i);
                            anak_name = listnamaanak.get(i);
                            anak_kelas = listkelasanak.get(i);
                            anak_foto = listfotoanak.get(i);
                        }
                    }
                    Log.d("idbranch",String.valueOf(idbranch));
                    Log.d("email", branch_email);
                    saveBranch(idbranch, branch_email, anak_id, anak_name, anak_kelas, anak_foto);


                    txtKelas.setText("Kelas : "+anak_kelas);

                    Picasso.with(act).load(BuildConfig.BASE_API_URL + "/" + anak_foto.replace("\\", "/"))
                            .error(R.drawable.loadimage)
                            .placeholder(R.drawable.loadimage)
                            .into(imgAnak);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
        
    }

    public void checkDevice(String token)
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = "aa";//tManager.getDeviceId();
        Call<Result<Update>> call = service.checkDevice(uuid,String.valueOf(BuildConfig.VERSION_NAME),Build.MANUFACTURER,android.os.Build.MODEL,"Android ",android.os.Build.VERSION.RELEASE,token);

        call.enqueue(new Callback<Result<Update>>() {
            @Override
            public void onResponse(Call<Result<Update>> call,Response<Result<Update>> response) {
                Log.d("Update Device", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    final Result<Update> result = response.body();
                    Log.d("Update Device", "response = " + new Gson().toJson(result));

                    if (result.getSuccess()) {
                        saveServerKey(result.getData().getGcm_server());
                        if(!result.getData().getVersion_code().equalsIgnoreCase(BuildConfig.VERSION_NAME) && !result.getData().getVersion_code().equalsIgnoreCase(""))
                        {
                            if(result.getData().getRequired())
                            {
                                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                builder.setMessage(result.getData().getDescription());

                                builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                                        try {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.lanayacomputindo.vcm.vcm_apps_layanacomputindo")));
//                                        } catch (android.content.ActivityNotFoundException anfe) {
//                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "com.lanayacomputindo.vcm.vcm_apps_layanacomputindo")));
//                                        }
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(result.getData().getUrl())));
                                    }
                                });

                                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });

                                dialog = builder.create();

                                dialog.show();

                                Button nbutton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                                nbutton.setTextColor(Color.RED);
                                Button pbutton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                                pbutton.setTextColor(getResources().getColor(R.color.warna_utama));
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), response.body().getData().getDescription(), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    else {
                        checkDevice("");
                        Intent i = new Intent(getApplicationContext(), SplashActivity.class);
                        startActivity(i);
                        finish();
                        savePreferences(false, false);
                        Log.e("Update Device", response.body().getMessage());
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Update Device", String.valueOf(response.raw().toString()));
                    //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<Update>> call,Throwable t) {
                Log.e("on Failure", t.toString());
                Log.d("update gcm gagal", "dicoba cek cek cek");
            }
        });
    }

    @Override
    protected void onPause() {
        //Saat onPause, BroadcastRecievernya akan di unregister.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public static void checkBranch()
    {
        pDialog = ProgressDialog.show(act,
                "Mengambil data anda",
                "Tunggu Sebentar!");

        RestClient.GitApiInterface service = RestClient.getClient(act);
        String with = "personClass.classRoom;branch";
        Call<Results<Person>> callanak = service.getAnakku(with);

        callanak.enqueue(new Callback<Results<Person>>() {
            @Override
            public void onResponse(Call<Results<Person>> call, Response<Results<Person>> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    pDialog.dismiss();
                    Results<Person> result = response.body();
                    Log.e("MainActivity", "response = " + new Gson().toJson(result));
                    if(result.getData().size()==0)
                    {
                        checkBranch = 1;
                        Intent f = new Intent(act, TambahAnakActivity.class);
                        act.startActivity(f);
                        Toast.makeText(act, "Silahkan Tambah anak anda terlebih dahulu", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        listidbranch.clear();
                        listemailbranch.clear();
                        listidanak.clear();
                        listnamaanak.clear();
                        listkelasanak.clear();
                        listfotoanak.clear();
                        saveBranch(result.getData().get(0).getBranch().getId(),
                                result.getData().get(0).getBranch().getEmail(),
                                result.getData().get(0).getId(),
                                result.getData().get(0).getName(),
                                getKelasData(result.getData().get(0).getPerson_class()),
                                result.getData().get(0).getPhoto());
                        for(int i=0; i<result.getData().size(); i++)
                        {
                            listidbranch.add(result.getData().get(i).getBranch().getId());
                            listemailbranch.add(result.getData().get(i).getBranch().getEmail());
                            listidanak.add(result.getData().get(i).getId());
                            listnamaanak.add(result.getData().get(i).getName());
                            listkelasanak.add(getKelasData(result.getData().get(i).getPerson_class()));
                            listfotoanak.add(result.getData().get(i).getPhoto());
                        }
                        getHeader();
                        checkBranch = 0;
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    pDialog.dismiss();
                    Log.e("on Failure", response.raw().message());

                }
            }

            @Override
            public void onFailure(Call<Results<Person>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                act.finish();
                Toast.makeText(act,R.string.cekkoneksi,Toast.LENGTH_LONG).show();

            }
        });

    }


    private static String getKelasData(List<PersonClass> personClass) {
        if (personClass.size()>0)
            return personClass.get(0).getClass_room().getName().toString();
        else
            return "";
    }


}
