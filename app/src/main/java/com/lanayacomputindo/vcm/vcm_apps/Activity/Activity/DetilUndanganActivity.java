package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Invitation;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetilUndanganActivity extends AppCompatActivity {

    private String bundleidundangan, bundlejudul, bundledeskripsi, bundletanggal;
    private Boolean bundlestatus;

    private TextView txtjudul, txttanggal, txtdeskripsi;

    private Boolean bundlemsg;

    ProgressDialog pDialog;

    private Switch swkehadiran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_undangan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle(getString(R.string.undangan));

        Bundle b = getIntent().getExtras();
        bundleidundangan = b.getString("id_undangan");
        bundlejudul = b.getString("judul");
        bundledeskripsi = b.getString("deskripsi");
        bundlestatus = b.getBoolean("status");
        bundletanggal = b.getString("tgl_create");

        txtjudul = (TextView) findViewById(R.id.judul);
        txttanggal = (TextView) findViewById(R.id.tanggal);
        txtdeskripsi = (TextView) findViewById(R.id.deskripsi);

        txtjudul.setText(bundlejudul);
        txttanggal.setText(bundletanggal);
        txtdeskripsi.setText(Html.fromHtml(bundledeskripsi));

        swkehadiran = (Switch) findViewById(R.id.switch1);
        swkehadiran.setChecked(true);
        if(bundlestatus)
        {
            swkehadiran.setChecked(true);
        }
        else
        {
            swkehadiran.setChecked(false);
        }

        swkehadiran.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                editKehadiran(isChecked);

            }
        });


    }

    public void editKehadiran(Boolean stat)
    {
        pDialog = ProgressDialog.show(DetilUndanganActivity.this,
                "Memproses kehadiran anda",
                "Tunggu Sebentar!");

        RestClient.GitApiInterface service = RestClient.getClient(this);

        String hadir = "";
        if(stat)
        {
            hadir = "1";
        }
        else
        {
            hadir = "0";
        }

        Log.d("kehadiran", hadir);
        Call<Result<Invitation>> call = service.updateInvitation(Integer.valueOf(bundleidundangan), hadir);

        call.enqueue(new Callback<Result<Invitation>>() {
            @Override
            public void onResponse(Call<Result<Invitation>> call, Response<Result<Invitation>> response) {
                pDialog.dismiss();
                Log.d("DetilUndangan", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Result<Invitation> result = response.body();
                    Log.d("DetilUndangan", "response = " + new Gson().toJson(result));

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                    Log.e("DetilUndangan", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Result<Invitation>> call, Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Log.d("gagal", "agagagagal");
                swkehadiran.setChecked(false);
                Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                DetilUndanganActivity.this.supportFinishAfterTransition();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
